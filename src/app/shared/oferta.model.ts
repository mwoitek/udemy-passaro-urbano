export class Oferta {
  public id = -1;
  public categoria = '';
  public titulo = '';
  public descricaoOferta = '';
  public anunciante = '';
  public valor = 0;
  public destaque = false;
  public imagens: Record<string, unknown>[] = [];
}
